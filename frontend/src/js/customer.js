$(document).ready(() => {
  // khai báo table
  let gCustomerTable = $('#table-customer').DataTable({
    columns: [
      { data: 'firstName' },
      { data: 'lastName' },
      { data: 'phoneNumber' },
      { data: 'address' },
      { data: 'city' },
      { data: 'state' },
      { data: 'postalCode' },
      { data: 'country' },
      { data: 'salesRepEmployeeNumber' },
      { data: 'creditLimit' },
      { data: 'Action' },
    ],
    columnDefs: [
      {
        targets: -1,
        defaultContent: `<i class="fas fa-user-edit text-primary"></i> | <i class="fas fa-user-minus text-danger"></i>`,
      },
    ],
  });
  // load dữ liệu vào bảng
  function loadDataOnTable(paramCustomer) {
    gCustomerTable.clear();
    gCustomerTable.rows.add(paramCustomer);
    gCustomerTable.draw();
  }

  // khai bao payment table
  let gOrderPaymentTable = $('#table-order-payment').DataTable({
    columns: [
      { data: 'orderDate' },
      { data: 'requiredDate' },
      { data: 'shippedDate' },
      { data: 'status' },
      { data: 'comments' },
      { data: 'checkNumber' },
      { data: 'paymentDate' },
      { data: 'ammount' },
    ],
  });
  function loadOrderPaymentToTable(paramOrderPayment) {
    gOrderPaymentTable.clear();
    gOrderPaymentTable.rows.add(paramOrderPayment);
    gOrderPaymentTable.draw();
  }

  // object customer
  let gCustomerId = 0;
  let gCustomer = {
    customerDb: '',
    // lấy customer về customer
    getAllCustomer() {
      $.ajax({
        url: `http://localhost:8080/api/customers`,
        method: 'get',
        async: false,
        success: (customers) => {
          gCustomer.customerDb = customers;
          loadDataOnTable(customers);
        },
        error: (err) => alert(err.responseText),
      });
    },
    // check phone number
    checkPhoneNumber(paramPhone) {
      return this.customerDb.some((customer) => customer.phoneNumber == paramPhone);
    },
    // khách hàng mới
    newCustomer: {
      lastName: '',
      firstName: '',
      phoneNumber: '',
      address: '',
      city: '',
      state: '',
      postalCode: '',
      country: '',
      salesRepEmployeeNumber: '',
      creditLimit: '',
    },
    // create customer
    onCreateCustomerClick() {
      $('#modal-save').modal('show');
      gCustomerId = 0;
      resetForm();
    },
    // updateCustomer
    onUpdateCustomerClick() {
      $('#modal-save').modal('show');
      let vSelectedRow = $(this).parents('tr');
      let vSelectedData = gCustomerTable.row(vSelectedRow).data();
      gCustomerId = vSelectedData.id;
      $.get(`http://localhost:8080/api/customers/${gCustomerId}`, loadCustomerToInput);
    },
    // save Customer
    onSaveCustomerClick() {
      this.newCustomer = {
        lastName: $('#input-customer-first-name').val().trim(),
        firstName: $('#input-customer-last-name').val().trim(),
        phoneNumber: $('#input-customer-phone').val().trim(),
        address: $('#input-customer-address').val().trim(),
        city: $('#input-customer-city').val().trim(),
        state: $('#input-customer-state').val().trim(),
        postalCode: $('#input-customer-postal-code').val().trim(),
        country: $('#input-customer-country').val().trim(),
        salesRepEmployeeNumber: $('#input-customer-sale-rep').val().trim(),
        creditLimit: $('#input-customer-credit-limit').val().trim(),
      };
      if (validateCustomer(this.newCustomer)) {
        gCustomer.saveCustomerInfo(this.newCustomer);
      }
    },
    saveCustomerInfo(paramCustomer) {
      if (gCustomerId == 0) {
        $.ajax({
          url: `http://localhost:8080/api/customers`,
          method: 'POST',
          contentType: `application/json; charset=utf-8`,
          data: JSON.stringify(paramCustomer),
          success: () => {
            alert(`successfully create new customer`);
            $('#modal-save').modal('hide');
            gCustomer.getAllCustomer();
          },
          error: (e) => alert(e.responseText),
        });
      } else {
        $.ajax({
          url: `http://localhost:8080/api/customers/${gCustomerId}`,
          method: 'put',
          contentType: `application/json`,
          data: JSON.stringify(paramCustomer),
          success: () => {
            $('#modal-save').modal('hide');
            alert(`successfully update customer`);
            gCustomer.getAllCustomer();
          },
          error: (e) => alert(e.responseText),
        });
      }
    },
    // delete customer
    onDeleteAllCustomerClick() {
      gCustomerId = 0;
      $('#modal-delete').modal('show');
    },
    onDeleteCustomerClick() {
      let vSelectedRow = $(this).parents('tr');
      let vSelectedData = gCustomerTable.row(vSelectedRow).data();
      gCustomerId = vSelectedData.id;
      $('#modal-delete').modal('show');
    },
    onConfirmDeleteCustomerClick() {
      if (gCustomerId == 0) {
        $.ajax({
          url: `http://localhost:8080/api/customers/`,
          method: 'delete',
          success: () => {
            alert('successfully delete all customer');
            $('#modal-delete').modal('hide');
            gCustomer.getAllCustomer();
          },
          error: (e) => alert(e.responseText),
        });
      } else {
        $.ajax({
          url: `http://localhost:8080/api/customers/${gCustomerId}`,
          method: 'delete',
          success: () => {
            alert('successfully delete all customer');
            $('#modal-delete').modal('hide');
            gCustomer.getAllCustomer();
          },
          error: (e) => alert(e.responseText),
        });
      }
    },
    // get order and payment
    onGetOrderAndPaymentClick(e) {
      let vCustomerId = $(e.target).data('id');
      $('#modal-payment-order').modal('show');
      $.get(
        `http://localhost:8080/api/customers/${vCustomerId}/orders/payment`,
        loadOrderPaymentToTable,
      );
    },
  };
  gCustomer.getAllCustomer();

  // add event listener
  $('#btn-create-customer').click(gCustomer.onCreateCustomerClick);
  $('#table-customer').on('click', '.fa-user-edit', gCustomer.onUpdateCustomerClick);
  $('#btn-save-info').click(gCustomer.onSaveCustomerClick);
  $('#btn-delete-all-customer').click(gCustomer.onDeleteAllCustomerClick);
  $('#table-customer').on('click', '.fa-user-minus', gCustomer.onDeleteCustomerClick);
  $('#table-customer').on('click', '.btn-get-order-payment', gCustomer.onGetOrderAndPaymentClick);
  $('#btn-confirm-delete').click(gCustomer.onConfirmDeleteCustomerClick);
  // validate customer
  function validateCustomer(paramCustomerInfo) {
    let vResult = true;
    try {
      if (paramCustomerInfo.firstName == '') {
        vResult = false;
        throw `100. first name can't be empty`;
      }
      if (paramCustomerInfo.lastName == '') {
        vResult = false;
        throw `200. last name can't be empty`;
      }
      if (paramCustomerInfo.phoneNumber == '') {
        vResult = false;
        throw `300. phone number can't be empty`;
      }
      if (gCustomerId == 0) {
        if (gCustomer.checkPhoneNumber(paramCustomerInfo.phoneNumber)) {
          vResult = false;
          throw `301. phone number is exist`;
        }
      }
    } catch (error) {
      alert(error);
    }
    return vResult;
  }

  // load Customer to input
  function loadCustomerToInput(paramCustomer) {
    $('#input-customer-first-name').val(paramCustomer.firstName);
    $('#input-customer-last-name').val(paramCustomer.lastName);
    $('#input-customer-phone').val(paramCustomer.phoneNumber);
    $('#input-customer-address').val(paramCustomer.address);
    $('#input-customer-city').val(paramCustomer.city);
    $('#input-customer-state').val(paramCustomer.state);
    $('#input-customer-postal-code').val(paramCustomer.postalCode);
    $('#input-customer-country').val(paramCustomer.country);
    $('#input-customer-sale-rep').val(paramCustomer.salesRepEmployeeNumber);
    $('#input-customer-credit-limit').val(paramCustomer.creditLimit);
  }

  // reset input
  function resetForm() {
    $('#input-customer-first-name').val('');
    $('#input-customer-last-name').val('');
    $('#input-customer-phone').val('');
    $('#input-customer-address').val('');
    $('#input-customer-city').val('');
    $('#input-customer-state').val('');
    $('#input-customer-postal-code').val('');
    $('#input-customer-country').val('');
    $('#input-customer-sale-rep').val('');
    $('#input-customer-credit-limit').val('');
  }
});
