$(document).ready(() => {
  $.get('http://localhost:8080/api/customers/countries', loadDataOnChart);
  function loadDataOnChart(paramData) {
    let vNumberCustomer = paramData.map((customer, index) => {
      return [index++, customer.numberCustomer];
    });
    let vCountry = paramData.map((country, index) => {
      return [index, country.country];
    });
    /*
     * BAR CHART
     * ---------
     */

    let bar_data = {
      data: vNumberCustomer,
      bars: { show: true },
    };
    $.plot('#customer-chart', [bar_data], {
      grid: {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor: '#f3f3f3',
      },
      series: {
        bars: {
          show: true,
          barWidth: 0.7,
          align: 'center',
        },
      },
      colors: ['#3c8dbc'],
      xaxis: {
        ticks: vCountry,
      },
    });
    /* END BAR CHART */
  }
});
