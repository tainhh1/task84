package com.devcamp.task84.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.task84.getrepository.GetCountryOfCustomer;
import com.devcamp.task84.model.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {
	@Query(value = "select * from customers c ", nativeQuery = true)
	List<Customer> getAllCustomer();

	@Query(value = "select c.country , count(c.id) numberCustomer \n" + "from customers c \n"
			+ "where c.country ='USA' or c.country ='France' or c.country ='Singapore' or c.country ='Spain'\n"
			+ "group by c.country ", nativeQuery = true)
	List<GetCountryOfCustomer> getCountryOfCustomers();
}
