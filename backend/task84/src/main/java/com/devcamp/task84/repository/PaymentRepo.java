package com.devcamp.task84.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.task84.model.Payment;

@Repository
public interface PaymentRepo extends JpaRepository<Payment, Integer> {
	List<Payment> findByCustomerId(Integer id);
}
