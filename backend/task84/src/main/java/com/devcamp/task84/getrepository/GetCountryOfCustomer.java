package com.devcamp.task84.getrepository;

public interface GetCountryOfCustomer {
	public String getCountry();
	public int getNumberCustomer();
}
