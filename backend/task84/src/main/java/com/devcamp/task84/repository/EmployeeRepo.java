package com.devcamp.task84.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.task84.model.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer> {
}
