package com.devcamp.task84.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task84.model.ProductLine;

public interface ProductLineRepo extends JpaRepository<ProductLine, Integer> {

}
